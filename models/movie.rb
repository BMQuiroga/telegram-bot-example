class Movie
  attr_reader :awards

  def initialize(name, adapter)
    json = adapter.get_movie_info(name)
    @name = name
    @awards = json['Awards']
  end
end

class MovieNotFound < StandardError
end

class MoviePrinter
  def self.print(awards)
    awards = 'Didnt win any awards' if awards == 'N/A'
    awards
  end

  def self.print_error
    'Was not found'
  end
end

class OMDBAdapter
  API_KEY = ENV['OMDB_API_KEY'].freeze
  URL = 'http://www.omdbapi.com/'.freeze
  def self.get_movie_info(name)
    response = Faraday.get(URL, { t: name, apikey: API_KEY })
    json = JSON.parse(response.body)
    raise MovieNotFound if json['Response'] == 'False'

    json
  end
end
