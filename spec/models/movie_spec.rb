require 'rspec'
require_relative '../../models/movie'

describe Movie do
  describe '#awards' do
    # rubocop:disable RSpec/ExampleLength
    it 'returns the awards of the movie' do
      response_body = {
        "Title": 'The Matrix',
        "Awards": 'Won 4 Oscars. 42 wins & 52 nominations total',
        "Response": 'True'
      }
      stub_request(:get, 'http://www.omdbapi.com/?apikey&t=The%20Matrix').with(
        headers: {
          'Accept' => '*/*',
          'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
          'User-Agent' => 'Faraday v2.7.4'
        }
      ).to_return(status: 200, body: response_body.to_json, headers: {})

      movie = described_class.new('The Matrix', OMDBAdapter)
      expect(movie.awards).to eq('Won 4 Oscars. 42 wins & 52 nominations total')
    end
    # rubocop:enable RSpec/ExampleLength
  end

  # rubocop:disable RSpec/ExampleLength
  it 'should throw an error if the movie does not exist' do
    response_body = {
      "Response": 'False',
      "Error": 'Movie not found!'
    }
    stub_request(:get, 'http://www.omdbapi.com/?apikey&t=The%20Matri').with(
      headers: {
        'Accept' => '*/*',
        'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
        'User-Agent' => 'Faraday v2.7.4'
      }
    ).to_return(status: 200, body: response_body.to_json, headers: {})

    expect { described_class.new('The Matri', OMDBAdapter) }.to raise_error(MovieNotFound)
  end
  # rubocop:enable RSpec/ExampleLength
end
