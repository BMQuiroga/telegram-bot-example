Feature: Consult movie awards

  @wip
  Background:
    Given that i'm in the chat of the telegram bot

  Scenario: Ask for the awards of a movie
    When i ask for the awards of the movie "The Godfather"
    Then i should receive the message "Won 3 Oscars. 31 wins & 31 nominations total"

  Scenario: Ask for the awards of a movie that has no awards
    When i ask for the awards of the movie "Emoji"
    Then i should receive the message "Didnt win any awards"

  Scenario: Ask for the awards of a movie that does not exist
    When i ask for the awards of the movie "asdasdasd"
    Then i should receive the message "Was not found"

  Scenario: Ask for the awards of multiple movies
    When i ask for the awards of the movies "The Godfather" and "Casablanca"
    Then i should receive the message "Won 3 Oscars. 31 wins & 31 nominations total"
    And i should receive the message "Won 3 Oscars. 14 wins & 11 nominations total"

